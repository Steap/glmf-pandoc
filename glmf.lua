-- that is very similar to that of pandoc's HTML writer.
-- There is one new feature: code blocks marked with class 'dot'
-- are piped through graphviz and images are included in the HTML
-- output using 'data:' URLs.
--
-- Invoke with: pandoc -t sample.lua
--
-- Note:  you need not have lua installed on your system to use this
-- custom writer.  However, if you do have lua installed, you can
-- use it to test changes to the script.  'lua sample.lua' will
-- produce informative error messages if your code contains
-- syntax errors.

-- Character escaping
local function escape(s, in_attribute)
  if string.match(s, "^%s+$") then
    return s
  end

  ret = s:gsub("[<>&\"']",
    function(x)
      if x == '<' then
        return '&lt;'
      elseif x == '>' then
        return '&gt;'
      elseif x == '&' then
        return '&amp;'
      elseif x == '"' then
        return '&quot;'
      elseif x == "'" then
        return '&#39;'
      else
        return x
      end 
    end)

  b, r = string.match(s, "(%s*)(.*)")
  l = b:len()
  if l > 0 then
    ret = '<text:s text:c="' .. l .. '"/>' .. ret
  end

  return ret
end

-- Helper function to convert an attributes table into
-- a string that can be put into HTML tags.
local function attributes(attr)
  local attr_table = {}
  for x,y in pairs(attr) do
    if y and y ~= "" then
      table.insert(attr_table, ' ' .. x .. '="' .. escape(y,true) .. '"')
    end
  end
  return table.concat(attr_table)
end

-- Run cmd on a temporary file containing inp and return result.
local function pipe(cmd, inp)
  local tmp = os.tmpname()
  local tmph = io.open(tmp, "w")
  tmph:write(inp)
  tmph:close()
  local outh = io.popen(cmd .. " " .. tmp,"r")
  local result = outh:read("*all")
  outh:close()
  os.remove(tmp)
  return result
end

-- Table to store footnotes, so they can be included at the end.
local notes = {}
local keywords = ''
local paragraphs = 0
local headers = 0

-- Blocksep is used to separate block elements.
function Blocksep()
  return "\n\n"
end

-- This function is called once for the whole document. Parameters:
-- body is a string, metadata is a table, variables is a table.
-- This gives you a fragment.  You could use the metadata table to
-- fill variables in a custom lua template.  Or, pass `--template=...`
-- to pandoc, and pandoc will add do the template processing as
-- usual.
function Doc(body, metadata, variables)
  body = string.gsub(body, 'KEYWORDS_PLACEHOLDER', metadata['keywords'])
  logos = ''
  print('------')
  for word in metadata['logos']:gmatch("[^,]*") do
  --for word in split(metadata['logos'], ",") do
    print(word)
    w = word:gsub("%s+", "")
    if #w > 0 then
      logos = logos .. Pragma('Logo : ' .. w)
    end
  end
  print('------')
  body = string.gsub(body, 'LOGOS_PLACEHOLDER', logos)
  local buffer = {}
  local function add(s)
    table.insert(buffer, s)
  end
  add(body)
  if #notes > 0 then
    add('<ol class="footnotes">')
    for _,note in pairs(notes) do
      add(note)
    end
    add('</ol>')
  end
  return table.concat(buffer,'\n') .. '\n'
end

-- The functions that follow render corresponding pandoc elements.
-- s is always a string, attr is always a table of attributes, and
-- items is always an array of strings (the items in a list).
-- Comments indicate the types of other variables.

function Str(s)
  return escape(s)
end

function Space()
  return " "
end

function SoftBreak()
  return ' '
end

function LineBreak()
  return "\n"
end

function Emph(s)
  return '<text:span text:style-name="italic">' .. s .. '</text:span>'
end

function Strong(s)
  return '<text:span text:style-name="gras">' .. s .. '</text:span>'
end

function Subscript(s)
  return ""
end

function Superscript(s)
  return ""
end

function SmallCaps(s)
  return ""
end

function Strikeout(s)
  return ""
end

function Link(s, src, tit, attr)
  return '<text:span text:style-name="url">' .. src .. '</text:span>'
end

function Image(s, src, tit, attr)
  return ""
end

function Code(s, attr)
  return '<text:span text:style-name="code_par">' .. escape(s) .. '</text:span>'
end

function InlineMath(s)
  return ""
end

function DisplayMath(s)
  return ""
end

function Note(s)
  return ""
end

function Span(s, attr)
  return ""
end

function Cite(s, cs)
  return ""
end

function Plain(s)
  return s
end

function Pragma(s)
    return '<text:p text:style-name="pragma">/// ' .. s .. ' ///</text:p>'
end

function Para(s)
  if paragraphs == 0 then
    print('XXX')
    print(keywords)
    print('XXX')
    paragraphs = 1
    ret =  '<text:p text:style-name="chapeau">' .. s .. '</text:p>'
    --ret = ret .. ' <text:p text:style-name="pragma">/// <text:span text:style-name="T2">Mots-clés ///</text:span></text:p>'
    ret = ret .. Pragma('Mots-clés')
    ret = ret .. Para('KEYWORDS_PLACEHOLDER')
    --ret = ret .. ' <text:p text:style-name="pragma">/// <text:span text:style-name="T2">Fin Mots-clés ///</text:span></text:p>'
    ret = ret .. Pragma('Fin mots-clés')
    ret = ret .. Para('')
    ret = ret .. ('LOGOS_PLACEHOLDER')
    return ret
  else
    return '<text:p text:style-name="P2">' .. s .. '</text:p>'
  end
end

-- lev is an integer, the header level.
function Header(lev, s, attr)
  --print("header" .. s)
  --if s == "L&#39;objectif" or s == 'Les outils' then
    return '<text:h text:style-name="Heading_20_' .. lev .. '"' ..
           ' text:outline-level="' .. lev .. '">' .. s .. '</text:h>'
  --else
   -- headers = headers + 1
    --return '<text:h text:style-name="Heading_20_' .. lev .. '"' ..
           --' text:outline-level="' .. lev .. '">Phase ' .. headers .. ' : ' .. s .. '</text:h>'
  --end
end

function BlockQuote(s)
  return ""
end

function HorizontalRule()
  return ""
end

function string:split( inSplitPattern, outResults )
 
   if not outResults then
      outResults = {}
   end 
   local theStart = 1 
   local theSplitStart, theSplitEnd = string.find( self, inSplitPattern,
theStart )
   while theSplitStart do
      table.insert( outResults, string.sub( self, theStart, theSplitStart-1 ) )      
      theStart = theSplitEnd + 1
      theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )     
   end
   --table.insert( outResults, string.sub( self, theStart ) )
   return outResults 
end

function CodeBlock(s, attr)
  ret = ''
  if attr.class and attr.class == 'console' then
    --for line in s:gmatch("[^\n]*") do
    for k, line in ipairs(s:split('\n')) do
      ret = ret .. '<text:p text:style-name="P11">' .. escape(line) .. '</text:p>' 
    end
  else
    --for line in s:gmatch("[^\r\n]+") do
    for k, line in ipairs(s:split('\n')) do
      ret = ret .. '<text:p text:style-name="P13">' .. escape(line) .. '</text:p>' 
    end
  end
  return ret
end

function BulletList(items)
  ret = ''
  for _, item in pairs(items) do
    ret = ret .. Para('- ' .. item .. '\n')
  end
  return ret
end

function OrderedList(items)
  ret = ''
  for k, item in pairs(items) do
    ret = ret .. Para(k .. ') ' .. item .. '\n')
  end
  return ret
end

-- Revisit association list STackValue instance.
function DefinitionList(items)
  return ""
end

-- Convert pandoc alignment to something HTML can use.
-- align is AlignLeft, AlignRight, AlignCenter, or AlignDefault.
function html_align(align)
  return ""
end

function CaptionedImage(src, tit, caption)
  return Pragma("Image : " .. src)
end

-- Caption is a string, aligns is an array of strings,
-- widths is an array of floats, headers is an array of
-- strings, rows is an array of arrays of strings.
function Table(caption, aligns, widths, headers, rows)
  return ""
end

function Div(s, attr)
  return ""
end

-- The following code will produce runtime warnings when you haven't defined
-- all of the functions you need for the custom writer, so it's useful
-- to include when you're working on a writer.
local meta = {}
meta.__index =
  function(_, key)
    io.stderr:write(string.format("WARNING: Undefined function '%s'\n",key))
    return function() return "" end
  end
setmetatable(_G, meta)

