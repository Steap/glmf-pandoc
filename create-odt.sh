#!/bin/sh

# Usage: create-odt.sh src result.odt
SRCDIR=$(pwd)/$1
OUT=$(pwd)/$2

echo "Using source from $SRCDIR"
echo "Writing $OUT"

echo "---------"

cd $SRCDIR && \
zip -q -0 -X $OUT mimetype && \
zip -q -r $OUT * -x mimetype

