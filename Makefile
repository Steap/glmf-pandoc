all: pandoc.odt

%.xml: %.org glmf.lua default.glmf
	pandoc -f org -t glmf.lua -o $@ $< \
	--template=default.glmf

%.odt: %.xml
	rm -f $@
	./unzip-odt.sh gnulinuxmagazine_recette.odt work
	cp $< work/content.xml
	cp meta.xml work/meta.xml
	./create-odt.sh work $@
	rm -rf work
	
