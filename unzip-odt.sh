#!/bin/sh

# Usage: unzip-odt.sh document.odt out-folder
unzip -q $1 -d $2
